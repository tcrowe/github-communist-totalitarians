# github-communist-totalitarians

The GitHub Code Communists' fight against diversity of thought.

🚫 They don't want you to know about: 🚫

+ Machine Learning Fairness (so-called)
+ GitHub works with ICE
+ Microsoft works with ICE
+ GitHub silence brown people that the U.S. Federal warmongers hate and kill

🙈 **And so much more!** 😵

![shut it down](./img/shut-it-down.png)

## Table of Contents

+ [tcrowe/code-communists](#tcrowecode-communists)
+ [tcrowe/nodejs-communist-problem](#tcrowenodejs-communist-problem)
+ [1995parham/github-do-not-ban-us](#1995parhamgithub-do-not-ban-us)
+ [Censorship of GitHub wiki](#censorship-of-github-wiki)
+ [Microsoft and GitHub employees protest Chinese labor conditions](#microsoft-and-github-employees-protest-chinese-labor-conditions)
+ [Copying, license, and contributing](#copying-license-and-contributing)

## tcrowe/code-communists

Mirrors:

+ <https://gitlab.com/tcrowe/code-communists>

Originally at <https://gitlab.com/tcrowe/code-communists> this repository contains investigations into many top tech companies political preferences.

![img/tcrowe-code-communists-disabled01.png](img/tcrowe-code-communists-disabled01.png)

## tcrowe/nodejs-communist-problem

Mirrors:

+ <https://gitlab.com/tcrowe/nodejs-communist-problem>

Originally at <https://gitlab.com/tcrowe/nodejs-communist-problem> this repository contains investigations about Node.js leadership that is filled up with hate.

![./img/tcrowe-nodejs-communist-problem-disabled01.png](./img/tcrowe-nodejs-communist-problem-disabled01.png)

## 1995parham/github-do-not-ban-us

 <https://github.com/1995parham/github-do-not-ban-us>

At the time of writing this the repository is back up for the time being.

## Censorship of GitHub wiki

<https://infogalactic.com/info/Censorship_of_GitHub>

## Microsoft and GitHub employees protest Chinese labor conditions

<https://www.businessinsider.com/microsoft-github-employees-stand-up-censorship-china-996-work-schedule-2019-4>

## Copying, license, and contributing

Copyright (C) Tony Crowe 2020 <https://tonycrowe.com/contact/>

Thank you for using and contributing to make github-communist-totalitarians better.

⚠️ Please run `npm run prd` before submitting a patch.

⚖️ github-communist-totalitarians is free and unlicensed. Anyone can use it, fork, or modify and we the community will try to help whenever possible.
